# Planning

## Research

* [ ] Vocab
* [ ] Functions
* [ ] Methods

## Problem decomposition

* [ ] Input
* [ ] Output
* [ ] Examples
* [ ] Conditions (if)
* [ ] Iteration (loop)

## Problems

### 01 minimum_value

#### use < > to compare parameters

### 04 max_of_three

### 06 can_skydive

### continue to plan each
