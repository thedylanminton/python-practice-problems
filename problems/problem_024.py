# Complete the calculate_average function which accepts
# a list of numerical values and returns the average of
# the numbers.
#
# If the list of values is empty, the function should
# return None
#
# Pseudocode is available for you

def calculate_average(values):
    sum = 0
    num_values = len(values)

    if values:
        for num in values:
            sum += num
        avg_values = sum / num_values
        print(avg_values)
        return avg_values
    else:
        print("None")
    return None


values = [1, 2, 3, 4, 5]
calculate_average(values)
