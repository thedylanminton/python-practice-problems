# Complete the minimum_value function so that returns the
# minimum of two values.
#
# If the values are the same, return either.

# Do some planning in ./planning.md

# Write out some pseudocode before trying to solve the
# problem to get a good feel for how to solve it.

def minimum_value(value1, value2):
    if value1 > value2:
        print(value2)
    elif value1 == value2:
        print("either")
    else:
        print(value1)


minimum_value(3, 9)
