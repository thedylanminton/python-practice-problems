# Complete the has_quorum function to test to see if the number
# of the people in the attendees list is greater than or equal
# to 50% of the number of people in the members list.

def has_quorum(attendees_list, members_list):
    if len(attendees_list) / len(members_list) >= 0.5:
        print("True")
        return True
    else:
        print("False")
        return False


list1 = ["adam", "jane", "jack"]
list2 =  ["adam", "jane"]
has_quorum(list1, list2)
