# Complete the can_skydive function so that determines if
# someone can go skydiving based on these criteria
#
# * The person must be greater than or equal to 18 years old, or
# * The person must have a signed consent form

# Do some planning in ./planning.md

# Write out some pseudocode before trying to solve the
# problem to get a good feel for how to solve it.

def can_skydive(age, has_consent_form):
    if age > 18:
        print("You're old enough to jump!")
    elif has_consent_form == True:
        print("You're permitted to jump!")
    else:
        print("You cannot jump.")


can_skydive(19, False)
can_skydive(16, True)
can_skydive(17, False)
