# Complete the remove_duplicate_letters that takes a string
# parameter "s" and returns a string with all of the
# duplicates removed.
#
# Examples:
#   * For "abc", the result is "abc"
#   * For "abcabc", the result is "abc"
#   * For "abccba", the result is "abc"
#   * For "abccbad", the result is "abcd"
#
# If the list is empty, then return the empty string.

def remove_duplicate_letters(s):
    removed_duplicates = ""
    for letter in s:
        if letter not in removed_duplicates:
            removed_duplicates += letter
        elif letter in removed_duplicates:
            pass
    return removed_duplicates


print(remove_duplicate_letters("aabbccdd"))
print(remove_duplicate_letters("abccd"))

# this doesn't work yet
