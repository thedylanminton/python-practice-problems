# Complete the calculate_sum function which accepts
# a list of numerical values and returns the sum of
# the numbers.
#
# If the list of values is empty, the function should
# return None
#

def calculate_sum(values):
    sum = 0
    if not values:
        print("None")
        return None
    else:
        for num in values:
            sum += num
        print(sum)
        return sum


values = [1, 2, 3, 4, 5]
calculate_sum(values)
