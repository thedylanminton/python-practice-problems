# Complete the check_password function that accepts a
# single parameter, the password to check.
#
# A password is valid if it meets all of these criteria
#   * It must have at least one lowercase letter (a-z)
#   * It must have at least one uppercase letter (A-Z)
#   * It must have at least one digit (0-9)
#   * It must have at least one special character $, !, or @
#   * It must have six or more characters in it
#   * It must have twelve or fewer characters in it
#
# The string object has some methods that you may want to use,
# like ".isalpha", ".isdigit", ".isupper", and ".islower"

def check_password(password):
    lc_letters = 0
    uc_letters = 0
    digits = 0
    special_characters = 0

    for char in password:
        if char.isalpha:
            if char.isupper:
                uc_letters += 1
            elif char.islower:
                lc_letters += 1
        elif char.isdigit:
            digits += 1
        elif char == "$" or char == "!" or char == "@":
            special_characters += 1

    if len(password) < 6 or len(password) > 12:
        return "invalid"
    elif lc_letters > 0 and uc_letters > 0 and digits > 0 and special_characters > 0:
        return "valid"
    else:
        return "invalid"
