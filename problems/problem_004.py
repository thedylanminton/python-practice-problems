# Complete the max_of_three function so that returns the
# maximum of three values.
#
# If two values are the same maximum value, return either of
# them.
# If the all of the values are the same, return any of them
#
# Use the >= operator for greater than or equal to

# Do some planning in ./planning.md

# Write out some pseudocode before trying to solve the
# problem to get a good feel for how to solve it.

def max_of_three(value1, value2, value3):
    numbers = [value1, value2, value3]
    if numbers[0] > numbers[1] and numbers[0] > numbers[2]:
        print(numbers[0])
    elif numbers[1] > numbers[0] and numbers[1] > numbers[2]:
        print(numbers[1])
    elif numbers[2] > numbers[0] and numbers[2] > numbers[1]:
        print(numbers[2])


max_of_three(3, 1, 2)
max_of_three(2, 3, 1)
max_of_three(2, 1, 3)
