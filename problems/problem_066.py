# Write a class that meets these requirements.
#
# Name:       Book
#
# Required state:
#    * author name, a string
#    * title, a string
#
# Behavior:
#    * get_author: should return "Author: «author name»"
#    * get_title:  should return "Title: «title»"
#
# Example:
#    book = Book("Natalie Zina Walschots", "Hench")
#
#    print(book.get_author())  # prints "Author: Natalie Zina Walschots"
#    print(book.get_title())   # prints "Title: Hench"
#
# There is pseudocode available for you to guide you


class Book:
    def __init__(self, author, title):
        self.author = author
        self.title = title

    def get_author(self):
        author = "Author: " + self.author
        return author

    def get_title(self):
        title = "Title: " + self.title
        return title


book = Book("Natalie Zina Walschots", "Hench")
print(book.get_author())
print(book.get_title())

book2 = Book("author name", "book title")
print(book2.get_author())
print(book2.get_title())
