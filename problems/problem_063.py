# Write a function that meets these requirements.
#
# Name:       shift_letters
# Parameters: a string containing a single word
# Returns:    a new string with all letters replaced
#             by the next letter in the alphabet
#
# If the letter "Z" or "z" appear in the string, then
# they would get replaced by "A" or "a", respectively.
#
# Examples:
#     * inputs:  "import"
#       result:  "jnqpsu"
#     * inputs:  "ABBA"
#       result:  "BCCB"
#     * inputs:  "Kala"
#       result:  "Lbmb"
#     * inputs:  "zap"
#       result:  "abq"
#
# You may want to look at the built-in Python functions
# "ord" and "chr" for this problem


def shift_letters(word):
    shifted_string = ""
    for char in word:
        char = ord(char)
        if char == 90:
            char = 65
            char = chr(char)
            shifted_string += char
        elif char == 122:
            char = 97
            char = chr(char)
            shifted_string += char
        else:
            char += 1
            char = chr(char)
            shifted_string += char
    return shifted_string


word = "zap"
print(shift_letters(word))
